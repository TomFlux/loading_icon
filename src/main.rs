use std::io::{stdout, Write};
use std::sync::mpsc;
use std::{thread, time};

// - \ | /
#[derive(Copy, Clone)]
enum LoadingBar {
    HBar,
    BSlash,
    VBar,
    FSlash,
}

impl LoadingBar {
    fn to_str(&self) -> String {
        match self {
            LoadingBar::HBar => String::from("-"),
            LoadingBar::BSlash => String::from("\\"),
            LoadingBar::VBar => String::from("|"),
            LoadingBar::FSlash => String::from("/"),
        }
    }

    fn get_next(&self) -> LoadingBar {
        match self {
            LoadingBar::HBar => LoadingBar::BSlash,
            LoadingBar::BSlash => LoadingBar::VBar,
            LoadingBar::VBar => LoadingBar::FSlash,
            LoadingBar::FSlash => LoadingBar::HBar,
        }
    }
}

fn print_next(curr_icon: LoadingBar, percentage: u32) -> std::io::Result<()> {
    print!(
        "\rLoading {}% {}",
        percentage.to_string(),
        curr_icon.to_str()
    );
    stdout().flush().ok().expect("could not flush std out"); // print! macro does not flush stdout

    Ok(())
}

fn fib_cpu_intensive(n: u32, transmitter: mpsc::Sender<u32>) -> u32 {
    let mut a = 0;
    let mut b = 1;
    let mut fib = 123;
    for i in 0..n {
        fib = a + b;
        a = b;
        b = fib;

        transmitter
            .send(((i as f32 / n as f32) * 100.0) as u32)
            .unwrap();

        thread::sleep(time::Duration::from_millis(1000));
    }

    fib
}

fn main() {
    let (kill_tx, kill_rx) = mpsc::channel();
    let (percent_tx, percent_rx) = mpsc::channel();

    println!("calculating fib sequence to 40 places...");
    thread::spawn(move || {
        let mut loading_bar = LoadingBar::HBar;
        let mut last_percentage = 0;
        loop {
            match kill_rx.try_recv() {
                Ok(_) | Err(mpsc::TryRecvError::Disconnected) => break,
                Err(mpsc::TryRecvError::Empty) => match percent_rx.try_recv() {
                    Ok(perc) => last_percentage = perc,
                    Err(mpsc::TryRecvError::Empty) => {}
                    Err(mpsc::TryRecvError::Disconnected) => {
                        println!("disconnected from percent transmitter");
                        break;
                    }
                },
            }

            match print_next(loading_bar, last_percentage) {
                Ok(()) => {
                    loading_bar = loading_bar.get_next();
                    thread::sleep(time::Duration::from_millis(100));
                }
                Err(_) => {
                    println!("Error printing percentage");
                    break;
                }
            }
        }
    });

    println!("\r{}              ", fib_cpu_intensive(40, percent_tx));

    kill_tx.send(()).unwrap();
}
